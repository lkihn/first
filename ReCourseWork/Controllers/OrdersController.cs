﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ReCourseWork.Models;
using System.Web.Script.Serialization;

namespace ReCourseWork.Controllers
{
    public class OrdersController : Controller
    {
        private Entities db = new Entities();


        private void BindDdls(int? aid = null, int? cid = null)
        {
            ViewBag.address_id = new SelectList(db.addresses.Select(a => new { id = a.address_id, label = a.city + ", " + a.street }), "id", "label", aid);
            ViewBag.customer_id = new SelectList(db.customers.Select(c => new { id = c.customer_id, label = c.fname + " " + c.lname }), "id", "label", cid);
        }



        // GET: Orders
        public ActionResult Index()
        {
            var orders = db.orders.Include(o => o.address).Include(o => o.customer);
            return View(orders.ToList());
        }

        // GET: Orders/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            order order = db.orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        // GET: Orders/Create
        public ActionResult Create()
        {
            BindDdls();
            JavaScriptSerializer jss = new JavaScriptSerializer();
            ViewBag.Products = jss.Serialize(db.products.Select(p => new { id = p.product_id, name = p.name }));
            return View();
        }

        // POST: Orders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "order_id,customer_id,address_id,shipment_date,is_shipped")] order order, [Bind(Include = "product_id,quantity")]List<order_item> orderItems)
        {
            order.added_date = DateTime.Now;
            foreach (var oi in orderItems)
                oi.order = order;
            if (ModelState.IsValid)
            {
                db.orders.Add(order);
                db.order_item.AddRange(orderItems);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            BindDdls(order.address_id, order.customer_id);
            return View(order);
        }

        // GET: Orders/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            order order = db.orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            BindDdls(order.address_id, order.customer_id);
            return View(order);
        }

        // POST: Orders/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "order_id,customer_id,address_id,added_date,shipment_date,is_shipped")] order order)
        {
            if (ModelState.IsValid)
            {
                db.Entry(order).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            BindDdls(order.address_id, order.customer_id);
            return View(order);
        }

        // GET: Orders/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            order order = db.orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        // POST: Orders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            order order = db.orders.Find(id);
            db.orders.Remove(order);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
