﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ReCourseWork.Models;

namespace ReCourseWork.Controllers
{
    public class OrderItemController : Controller
    {
        private Entities db = new Entities();

        // GET: OrderItem
        public ActionResult Index()
        {
            var order_item = db.order_item.Include(o => o.order).Include(o => o.product);
            return View(order_item.ToList());
        }

        // GET: OrderItem/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            order_item order_item = db.order_item.Find(id);
            if (order_item == null)
            {
                return HttpNotFound();
            }
            return View(order_item);
        }

        // GET: OrderItem/Create
        public ActionResult Create()
        {
            ViewBag.order_id = new SelectList(db.orders, "order_id", "order_id");
            ViewBag.product_id = new SelectList(db.products, "product_id", "name");
            return View();
        }

        // POST: OrderItem/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "order_item_id,order_id,product_id,quantity")] order_item order_item)
        {
            if (ModelState.IsValid)
            {
                db.order_item.Add(order_item);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.order_id = new SelectList(db.orders, "order_id", "order_id", order_item.order_id);
            ViewBag.product_id = new SelectList(db.products, "product_id", "name", order_item.product_id);
            return View(order_item);
        }

        // GET: OrderItem/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            order_item order_item = db.order_item.Find(id);
            if (order_item == null)
            {
                return HttpNotFound();
            }
            ViewBag.order_id = new SelectList(db.orders, "order_id", "order_id", order_item.order_id);
            ViewBag.product_id = new SelectList(db.products, "product_id", "name", order_item.product_id);
            return View(order_item);
        }

        // POST: OrderItem/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "order_item_id,order_id,product_id,quantity")] order_item order_item)
        {
            if (ModelState.IsValid)
            {
                db.Entry(order_item).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.order_id = new SelectList(db.orders, "order_id", "order_id", order_item.order_id);
            ViewBag.product_id = new SelectList(db.products, "product_id", "name", order_item.product_id);
            return View(order_item);
        }

        // GET: OrderItem/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            order_item order_item = db.order_item.Find(id);
            if (order_item == null)
            {
                return HttpNotFound();
            }
            return View(order_item);
        }

        // POST: OrderItem/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            order_item order_item = db.order_item.Find(id);
            db.order_item.Remove(order_item);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
