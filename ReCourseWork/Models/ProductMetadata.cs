﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ReCourseWork.Models
{
    public class ProductMetadata
    {
        [Required,
        MaxLength(20),
        Display(Name = "Name")]
        public string name { get; set; }

        [Required,
        Range(0, int.MaxValue),
        Display(Name = "Available")]
        public int quantity { get; set; }

        [Required,
        Range(0, double.MaxValue),
        Display(Name = "Unit price")]
        public double price_per_item { get; set; }
    }

    [MetadataType(typeof(ProductMetadata))]
    public partial class product { }
}