﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ReCourseWork.Models
{
    public class AddressMetadata
    {
        [Display(Name = "Client"),
        Range(1, int.MaxValue)]
        public int customer_id { get; set; }

        [Display(Name = "Client")]
        public customer customer { get; set; }

        [Display(Name = "City"),
        MaxLength(15),
        Required]
        public string city { get; set; }

        [Display(Name = "Address"),
        MaxLength(20),
        Required]
        public string street { get; set; }
    }

    [MetadataType(typeof(AddressMetadata))]
    public partial class address
    {
    }
}