﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ReCourseWork.Models
{
    public class CustomerMetadata
    {
        [Display(Name = "Name"),
        MaxLength(10),
        Required]
        public string fname { get; set; }

        [Display(Name = "Surname"),
        MaxLength(10),
        Required]
        public string lname { get; set; }

        [Display(Name = "Email"),
        MaxLength(20),
        EmailAddress]
        public string email { get; set; }

        [Display(Name = "Telephone"),
        MaxLength(20),
        Required]
        public string phone { get; set; }
    }

    [MetadataTypeAttribute(typeof(CustomerMetadata))]
    public partial class customer
    {
    }
}