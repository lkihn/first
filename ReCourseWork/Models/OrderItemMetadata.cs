﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ReCourseWork.Models
{
    public class OrderItemMetadata
    {
        [Display(Name = "Количество"),
        Range(0, int.MaxValue)]
        public int quantity { get; set; }
    }

    [MetadataType(typeof(OrderItemMetadata))]
    public partial class order_item { }
}