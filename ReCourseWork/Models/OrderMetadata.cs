﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ReCourseWork.Models
{
    public class OrderMetadata
    {
        [Display(Name = "Client"),
        Range(1, int.MaxValue)]
        public int customer_id { get; set; }

        [Display(Name = "Client")]
        public customer customer { get; set; }

        [Display(Name = "Address")]
        public address address { get; set; }

        [Display(Name = "Address"),
        Range(1, int.MaxValue)]
        public int address_id { get; set; }

        [Display(Name = "Added date")]
        public DateTime added_date { get; set; }

        [Display(Name = "Shipment date")]
        public DateTime shipment_date { get; set; }

        [Display(Name = "Is shipped")]
        public bool is_shipped { get; set; }
    }

    [MetadataType(typeof(OrderMetadata))]
    public partial class order { }
}