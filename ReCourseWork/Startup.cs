﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ReCourseWork.Startup))]
namespace ReCourseWork
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
